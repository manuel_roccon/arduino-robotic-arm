/*******Author: Roccon Manuel - manuel.roccon@gmail.com********************/
/************Last modify: 18/10/2017***********/
/************************************************/
//for corrert use of this script must insert this library in arduino ide
#include <Servo.h>
#include <EEPROM.h>
#include <OneWire.h>

Servo servoX;//create servo object to control a servo
Servo servoY;//create servo object to control a servo
Servo servoZ;//create servo object to control a servo
Servo servoPinza;//create servo object to control a servo

int pinServoX = 8;
int pinServoY = 9;
int pinServoZ = 10;
int pinServoPinza = 11;

int pinJoyX = 0;
int pinJoyY = 1;
int pinJoyZ = 2;
int pinJoyPinza = 2;

int xPosition = 0;
int yPosition = 0;
int zPosition = 0;
int pinzaPosition = 0;

int xVal = 0;
int yVal = 0;
int zVal = 0;

int degreeY;
int basicY;
int basicYtollerance;

int degreeX;
int basicX;
int basicXtollerance;

int degreePinza;

int degreeZ;

int switchState ;

long interval = 1000;           // interval to shift
unsigned long previousMillis = 0;        // will store last time LED was updated

int defaultData; //Setting a variable for default potentiometer data on start 

int pinzaStatus = 1;

int pres = 0;

int pinzaOpen = 70; //closed
int pinzaClosed = 100; //open

void setup() {
  // initialize serial communications at 9600 bps:
  pinMode(pinServoX, INPUT);
  pinMode(pinServoY, INPUT);
  pinMode(pinServoZ, INPUT);
  pinMode(pinServoPinza, INPUT);
  pinMode(pinJoyPinza, INPUT);
  digitalWrite(pinJoyPinza, HIGH);
  
  Serial.begin(9600); 
  // put your setup code here, to run once:
  servoX.attach(pinServoX);//attachs the servo on pin 9 to servo object
  servoY.attach(pinServoY);//attachs the servo on pin 9 to servo object
  servoZ.attach(pinServoZ);//attachs the servo on pin 9 to servo object
  servoPinza.attach(pinServoPinza);
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
  degreeY = 90;
  basicY=500;
  basicYtollerance = 30;

  degreeX = 130;
  basicX=500;
  basicXtollerance = 30;

  degreeZ = 90;

  degreePinza = 90;

  servoPinza.write(pinzaClosed);
}

void loop() {
  yPosition = analogRead(pinJoyY);
  Serial.print("Y: ");
  Serial.print(yPosition);
  Serial.print(" | DEG: ");
  Serial.print(degreeY);
  //degreeY = map (zPosition, 0, 1023, 0 ,255);
  if(basicY>yPosition + basicYtollerance && degreeY>=0){
    degreeY--;
    degreeY--;
  }
  if(basicY<yPosition - basicYtollerance && degreeY<=140){
    degreeY++;
    degreeY++;
  }
    servoY.write(degreeY);

  xPosition = analogRead(pinJoyX);
  Serial.print(" X: ");
  Serial.print(xPosition);
  Serial.print(" | DEG: ");
  Serial.print(degreeX);
  
  if(basicX>xPosition + basicXtollerance && degreeX>30){
    degreeX--;
    degreeX--;
    
  }
  if(basicX<xPosition - basicXtollerance && degreeX<180){
    degreeX++;
    degreeX++;
  }
  
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off 
  //delay(50);                       // wait for half a second
 servoX.write(degreeX);

  zPosition = analogRead(pinJoyZ);
  Serial.print(" Z: ");
  Serial.print(zPosition);
  Serial.print(" | DEG: ");
  Serial.print(degreeZ);
  
  if(basicX>zPosition + basicXtollerance){
    degreeZ++;
    degreeZ++;
    
  }
  if(basicX<zPosition - basicXtollerance){
    degreeZ--;
    degreeZ--;
  }
  servoZ.write(degreeZ);
  
  switchState = digitalRead(pinJoyPinza);
  Serial.print(switchState);

  if (switchState == LOW)
  {
    Serial.println("Switch = Pressed");
    if(servoPinza.read()==pinzaClosed && pres == 0){
      servoPinza.write(pinzaOpen);
      
    }else{
      if(servoPinza.read()==pinzaOpen && pres == 0){
        servoPinza.write(pinzaClosed);
      }
    }
    pres=1;
  }
  else{
    Serial.println("Switch = Nor Pressed");
    pres=0;
  }
  
  Serial.println();
  Serial.flush();
}
