/*******Author: Roccon Manuel - manuel.roccon@gmail.com********************/
/************Last modify: 18/10/2017***********/
/************************************************/
//for corrert use of this script must insert this library in arduino ide

#include <EEPROM.h>
#include <OneWire.h>
#include <LiquidCrystal_I2C.h>
#include <VarSpeedServo.h>

VarSpeedServo servoX;//create servo object to control a servo
VarSpeedServo servoY;//create servo object to control a servo
VarSpeedServo servoZ;//create servo object to control a servo
VarSpeedServo servoPinza;//create servo object to control a servo



int pinServoX = 8;
int pinServoY = 9;
int pinServoZ = 10;
int pinServoPinza = 11;

int pinJoyX = 0;
int pinJoyY = 1;
int pinJoyZ = 2;
int pinJoyPinza = 2;

int xPosition = 0;
int yPosition = 0;
int zPosition = 0;
int pinzaPosition = 0;

int xVal = 0;
int yVal = 0;
int zVal = 0;

int degreeY;
int basicY;
int basicYtollerance;

int degreeX;
int basicX;
int basicXtollerance;

int degreePinza;

int degreeZ;

int switchState ;

long interval = 1000;           // interval to shift
unsigned long previousMillis = 0;        // will store last time LED was updated

int defaultData; //Setting a variable for default potentiometer data on start

int pinzaStatus = 1;

int pres = 0;

int pinzaOpen = 70; //closed
int pinzaClosed = 100; //open

//programming variables
LiquidCrystal_I2C lcd(0x27, 16, 2);
typedef struct MYELEMENT {
  public:
    int servoX;
    int servoY;
    int servoZ;
    int servoPinza;
} MYELEMENT;
#define NUM_ELEMENTS 60
MYELEMENT ROBOPOS[NUM_ELEMENTS];
int cont = 0;
int programSw = 7;
int hitSw = 5;

void setup() {
  lcd.init();                      // initialize the lcd
  lcd.backlight();

  lcd.home();

  lcd.setCursor(0, 0);
  lcd.print("RoboArm V1");
  lcd.setCursor(0, 1);
  lcd.print("Manuel Roccon");

  // initialize serial communications at 9600 bps:
  pinMode(pinServoX, INPUT);
  pinMode(pinServoY, INPUT);
  pinMode(pinServoZ, INPUT);
  pinMode(pinJoyPinza, INPUT);
  pinMode(programSw, INPUT);
  pinMode(hitSw, INPUT);
  pinMode(pinServoPinza, INPUT);
  digitalWrite(pinJoyPinza, HIGH);
  digitalWrite(programSw, HIGH);
  digitalWrite(hitSw, HIGH);

  Serial.begin(9600);
  // put your setup code here, to run once:
  servoX.attach(pinServoX);//attachs the servo on pin 9 to servo object
  servoY.attach(pinServoY);//attachs the servo on pin 9 to servo object
  servoZ.attach(pinServoZ);//attachs the servo on pin 9 to servo object
  servoPinza.attach(pinServoPinza);
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
  degreeY = 90;
  basicY = 500;
  basicYtollerance = 30;

  degreeX = 130;
  basicX = 500;
  basicXtollerance = 30;

  degreeZ = 90;

  degreePinza = 90;

  servoPinza.write(pinzaOpen);
}
int programState = 0;
int pressa = 0;
int pressb = 0;
void loop() {

  lcd.setCursor(14, 0);
  lcd.print(cont);

  if (digitalRead(programSw) == LOW && pressa == 0)
  {
    lcd.clear();
    lcd.setCursor(0, 0);
    if (programState == 0) {
      lcd.print("PROGRAMMING");
      programState = 1;
      pressa = 1;
      cont = 0;
      lcd.setCursor(0, 1);
      lcd.print("CMD CLEARED");
    } else {
      if (programState == 1) {
        lcd.print("MANUAL MODE");
        programState = 0;
        pressa = 1;
      }
    }
  } else {
    pressa = 0;
  }

  if (digitalRead(hitSw) == LOW && programState == 1 && pressb == 0)
  {
    lcd.setCursor(0, 1);
    ROBOPOS[cont].servoX = servoX.read();
    ROBOPOS[cont].servoY = servoY.read();
    ROBOPOS[cont].servoZ = servoZ.read();
    ROBOPOS[cont].servoPinza = servoPinza.read();
    lcd.print(String("P") + String(cont + 1) + String(" ") + String(ROBOPOS[cont].servoX) + String(" ") + String(ROBOPOS[cont].servoY) + String(" ") + String(ROBOPOS[cont].servoZ));
    cont = cont + 1;
    pressb = 1;
  } else {
    pressb = 0;
    if (digitalRead(hitSw) == LOW && programState == 0 && pressb == 0) {
      if (cont == 0) {
        lcd.setCursor(0, 1);
        lcd.print("NO PROGRAM DATA");
      } else {
        lcd.setCursor(0, 0);
        lcd.print("AUTO MODE   ");
        lcd.setCursor(0, 1);
        lcd.print("START");
        lcd.setCursor(0, 1);
        for (int i = 0; i < cont; i++) {
          lcd.setCursor(0, 1);
          lcd.print(String("P") + String(i + 1) + String(" ") + String(ROBOPOS[i].servoX) + String(" ") + String(ROBOPOS[i].servoY) + String(" ") + String(ROBOPOS[i].servoZ));
          servoX.write(ROBOPOS[i].servoX, 12);
          servoY.write(ROBOPOS[i].servoY, 12);
          servoZ.write(ROBOPOS[i].servoZ, 12);
          servoPinza.write(ROBOPOS[i].servoPinza, 180);
          servoX.wait(); // wait for servo 1 to finish
          servoY.wait();  // wait for servo 2 to finish
          servoZ.wait();  // wait for servo 2 to finish
          delay(500);
          degreeX = ROBOPOS[cont - 1].servoX;
          degreeY = ROBOPOS[cont - 1].servoY;
          degreeZ = ROBOPOS[cont - 1].servoZ;
        }
        lcd.setCursor(0, 0);
        lcd.print("MANUAL MODE");
        lcd.setCursor(0, 1);
        lcd.print("COMPLETED       ");
      }
    } else {
      pressb = 0;
    }
  }

  yPosition = analogRead(pinJoyY);
  Serial.print("Y: ");
  Serial.print(yPosition);
  Serial.print(" | DEG: ");
  Serial.print(degreeY);
  //degreeY = map (zPosition, 0, 1023, 0 ,255);
  if (basicY > yPosition + basicYtollerance && degreeY >= 0) {
    degreeY--;
    degreeY--;
  }
  if (basicY < yPosition - basicYtollerance && degreeY <= 140) {
    degreeY++;
    degreeY++;
  }
  servoY.write(degreeY);

  xPosition = analogRead(pinJoyX);
  Serial.print(" X: ");
  Serial.print(xPosition);
  Serial.print(" | DEG: ");
  Serial.print(degreeX);

  if (basicX > xPosition + basicXtollerance && degreeX > 30) {
    degreeX--;
    degreeX--;

  }
  if (basicX < xPosition - basicXtollerance && degreeX < 180) {
    degreeX++;
    degreeX++;
  }

  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off
  //delay(50);                       // wait for half a second
  servoX.write(degreeX);

  zPosition = analogRead(pinJoyZ);
  Serial.print(" Z: ");
  Serial.print(zPosition);
  Serial.print(" | DEG: ");
  Serial.print(degreeZ);

  if (basicX > zPosition + basicXtollerance) {
    degreeZ++;
    degreeZ++;

  }
  if (basicX < zPosition - basicXtollerance) {
    degreeZ--;
    degreeZ--;
  }
  servoZ.write(degreeZ);

  switchState = digitalRead(pinJoyPinza);
  Serial.print(switchState);

  if (switchState == LOW)
  {
    Serial.println("Switch = Pressed");
    if (servoPinza.read() == pinzaClosed && pres == 0) {
      servoPinza.write(pinzaOpen);

    } else {
      if (servoPinza.read() == pinzaOpen && pres == 0) {
        servoPinza.write(pinzaClosed);
      }
    }
    pres = 1;
  }
  else {
    Serial.println("Switch = Nor Pressed");
    pres = 0;
  }

  Serial.println();
  Serial.flush();
}
